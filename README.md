# SuperBLT LuaSocket

LuaSocket, wrapped into a SuperBLT plugin

## Licence

This project is licenscd under the GNU General Public Licence, version 3 (or later). A copy of this
licence is included in the form of `LICENSE.txt`.

This project contains a modified copy of LuaSocket - it's licence can be found in `LICENCE-LUASOCKET.txt`.
